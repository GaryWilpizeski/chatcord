# ChatCord App
Realtime chat app with websockets using Node.js, Express and Socket.io with Vanilla JS on the frontend with a custom UI

Based on [Realtime Chat With Users & Rooms - Socket.io, Node & Express](https://www.youtube.com/watch?v=jD7FnbI76Hg) by [Brad Traversy](https://www.traversymedia.com/).

## Usage
```
npm install
npm run dev

Go to localhost:3000
```
